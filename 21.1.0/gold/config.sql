
update
	public.system_config
set
	value = '10,'
	where
	"key" = 'telehealthApptTypeIds';

UPDATE public.system_config
SET value='{
	"coverageOption": [
		{
			"id": "1",
			"coverageValue": "This is work related condition, covered by workers compensation plan",
			"coverageDescription": "Additional notes for work comp",
			"check": false
		},
		{
			"id": "2",
			"coverageValue": "Will pay cash and not use insurance",
			"coverageDescription": "You will be charged a fee of $200 for new appointment",
			"check": false
		},
		{
			"id": "3",
			"coverageValue": "Will use health insurance",
			"coverageDescription": "Clinic does not accept XYZ plan",
			"check": false
		}
	],
	"showInsuranceForm": [
		"3"
	],
	"stopBooking": [
		""
	]
}'
WHERE "key"='INSURANCE_COVERAGE_CONFIG';


update
	public.system_code_detail
set
	system_code_value_desc = 'Spanish',	
	flag2 = 1
	where
	system_code_id = '1100'
	and system_code_type = '04'
	and system_code_value = 'es';

update
	public.system_code_detail
set
	flag2 = 1,
	is_active = 'Y'
where
	system_code_id = '1100'
	and system_code_type = '04'
	and system_code_value = 'en';

	
INSERT INTO system_code_header(system_code_id, system_code_type, system_code_description, created_date, created_by_user_id, updated_date, updated_by_user_id, eff_end_ts, is_factory_setting, is_active, system_code_notes)
select '1400', '30', 'Screen flows', now(), 'sshirale', now(), 'sshirale', 'infinity', 'Y', 'Y', 'These screen flows will be used for Patient scheduling, Rescheduling, cancellation, Referral appointment booking and other eid based operations.'
where NOT EXISTS (SELECT 1 from system_code_header where eff_end_ts > now() and system_code_id ='1400' and system_code_type='30' );

INSERT INTO system_code_detail(system_code_id, system_code_type, system_code_value, system_code_value_desc, flag1, flag2, flag3, flag4, flag5, number1, number2, number3, number4, number5, text1, text2, text3, text4, text5, flag1_description, flag2_description, flag3_description, flag4_description, flag5_description, number1_description, number2_description, number3_description, number4_description, number5_description, text1_description, text2_description, text3_description, text4_description, text5_description, created_date, created_by_user_id, updated_date, updated_by_user_id, eff_end_ts, long_description, is_factory_setting, is_active)
select '1400', '30', 'SCHEDULE', '{"name": "Referral Management Flow"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{ 	"config": { 		"scheduling_flow": "referral", 		"is_workflow_disabled": true, 		"is_reschedule_cancel_panel_visible": false, 		"is_need_more_options_visible": false, 		"is_patient_instruction_visible": true, 		"hide_until_alternate_selected": true, 		"show_covid_notification": false, 		"is_map_visible": true, 		"is_calendar_options_visible": true, 		"show_earliest_availibility_header": false 	}, 	"data": [ 		{ 			"path": "details", 			"title": "CLINICAL DETAILS", 			"previous": "Previous", 			"next": "Next" 		}, 		{ 			"path": "booking", 			"title": "BOOK AN APPOINTMENT", 			"previous": "Previous", 			"next": "Next" 		}, 		{ 			"path": "confirm", 			"terminal": true 		} 	] }', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, now(), 'sshirale', now(), 'sshirale', 'infinity', NULL, 'N', 'Y'
where NOT EXISTS (SELECT 1 from system_code_detail where eff_end_ts > now() and system_code_id ='1400' and system_code_type='30' and system_code_value = 'SCHEDULE');

INSERT INTO system_code_detail(system_code_id, system_code_type, system_code_value, system_code_value_desc, flag1, flag2, flag3, flag4, flag5, number1, number2, number3, number4, number5, text1, text2, text3, text4, text5, flag1_description, flag2_description, flag3_description, flag4_description, flag5_description, number1_description, number2_description, number3_description, number4_description, number5_description, text1_description, text2_description, text3_description, text4_description, text5_description, created_date, created_by_user_id, updated_date, updated_by_user_id, eff_end_ts, long_description, is_factory_setting, is_active)
select '1400', '30', 'RESCHEDULE', '{"name": "Rescheduling flow"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{ 	"config": { 		"scheduling_flow": "reschedule", 		"is_workflow_disabled": true, 		"is_reschedule_cancel_panel_visible": false, 		"is_need_more_options_visible": false, 		"is_patient_instruction_visible": true, 		"hide_until_alternate_selected": false, 		"show_covid_notification": false, 		"is_map_visible": true, 		"is_calendar_options_visible": true, 		"show_earliest_availibility_header": false 	}, 	"data": [ 		{ 			"path": "details", 			"title": "CLINICAL DETAILS", 			"previous": "Previous", 			"next": "Next" 		}, 		{ 			"path": "booking", 			"title": "BOOK AN APPOINTMENT", 			"previous": "Previous", 			"next": "Next" 		}, 		{ 			"path": "confirm", 			"terminal": true 		} 	] }', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, now(), 'sshirale', now(), 'sshirale', 'infinity', NULL, 'N', 'Y'
where NOT EXISTS (SELECT 1 from system_code_detail where eff_end_ts > now() and system_code_id ='1400' and system_code_type='30' and system_code_value = 'RESCHEDULE');

INSERT INTO system_code_detail(system_code_id, system_code_type, system_code_value, system_code_value_desc, flag1, flag2, flag3, flag4, flag5, number1, number2, number3, number4, number5, text1, text2, text3, text4, text5, flag1_description, flag2_description, flag3_description, flag4_description, flag5_description, number1_description, number2_description, number3_description, number4_description, number5_description, text1_description, text2_description, text3_description, text4_description, text5_description, created_date, created_by_user_id, updated_date, updated_by_user_id, eff_end_ts, long_description, is_factory_setting, is_active)
select '1400','30','DEFAULT','{"name": "DASHSelf entire flow"}',null,null,null,null,null,null,null,null,null,null,'{    "config": {        "hide_until_alternate_selected": false,        "show_covid_notification": true,        "show_earliest_availibility_header": true     },    "data": [        {            "path": "search",            "title": "SEARCH INFORMATION",            "previous": "Previous",            "next": "Next"        },        {            "path": "patient",            "title": "PATIENT INFORMATION",            "previous": "Previous",            "next": "Next"        },        {            "path": "insurance-option",            "title": "INSURANCE OPTIONS",            "previous": "Previous",            "next": "Next"        },        {            "path": "insurance",            "title": "INSURANCE INFORMATION",            "previous": "Previous",            "next": "Next"        },        {            "path": "details",            "title": "CLINICAL DETAILS",            "previous": "Previous",            "next": "Next"        },        {            "path": "booking",            "title": "BOOK AN APPOINTMENT",            "previous": "Previous",            "next": "Next"        },        {            "path": "demograhics",            "title": "DEMOGRAPHICS",            "previous": "Previous",            "next": "Next"        },        {            "path": "confirm",            "terminal": true        }    ]}',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,now(),'sshirale',now(),'sshirale','infinity',null,'N','Y'
where NOT EXISTS (SELECT 1 from system_code_detail where eff_end_ts > now() and system_code_id ='1400' and system_code_type='30' and system_code_value = 'DEFAULT');

INSERT INTO system_code_detail(system_code_id, system_code_type, system_code_value, system_code_value_desc, flag1, flag2, flag3, flag4, flag5, number1, number2, number3, number4, number5, text1, text2, text3, text4, text5, flag1_description, flag2_description, flag3_description, flag4_description, flag5_description, number1_description, number2_description, number3_description, number4_description, number5_description, text1_description, text2_description, text3_description, text4_description, text5_description, created_date, created_by_user_id, updated_date, updated_by_user_id, eff_end_ts, long_description, is_factory_setting, is_active)
select '1400', '30', 'CONFIRM', '{"name" : "Confirmation Flow"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{ 	"config": { 		"scheduling_flow": "confirm", 		"is_reschedule_cancel_panel_visible": false, 		"is_need_more_options_visible": false, 		"is_patient_instruction_visible": true, 		"hide_until_alternate_selected": false, 		"show_covid_notification": false, 		"show_earliest_availibility_header": false, 		"is_map_visible": true, 		"is_calendar_options_visible": true 	}, 	"data": [ 		{ 			"path": "confirm", 			"terminal": true 		} 	] }', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, now(), 'sshirale', now(), 'sshirale', 'infinity', NULL, 'N', 'Y'
where NOT EXISTS (SELECT 1 from system_code_detail where eff_end_ts > now() and system_code_id ='1400' and system_code_type='30' and system_code_value = 'CONFIRM');

INSERT INTO system_code_detail(system_code_id, system_code_type, system_code_value, system_code_value_desc, flag1, flag2, flag3, flag4, flag5, number1, number2, number3, number4, number5, text1, text2, text3, text4, text5, flag1_description, flag2_description, flag3_description, flag4_description, flag5_description, number1_description, number2_description, number3_description, number4_description, number5_description, text1_description, text2_description, text3_description, text4_description, text5_description, created_date, created_by_user_id, updated_date, updated_by_user_id, eff_end_ts, long_description, is_factory_setting, is_active)
select '1400', '30', 'CANCEL', '{"name" : "Cancellation Flow"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{ 	"config": { 		"scheduling_flow": "cancel", 		"is_workflow_disabled": true, 		"is_reschedule_cancel_panel_visible": false, 		"is_need_more_options_visible": false, 		"is_patient_instruction_visible": false, 		"hide_until_alternate_selected": false, 		"show_covid_notification": false, 		"show_earliest_availibility_header": false, 		"is_map_visible": false, 		"is_calendar_options_visible": false 	}, 	"data": [ 		{ 			"path": "confirm", 			"terminal": true 		} 	] }', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, now(), 'sshirale', now(), 'sshirale', 'infinity', NULL, 'N', 'Y'
where NOT EXISTS (SELECT 1 from system_code_detail where eff_end_ts > now() and system_code_id ='1400' and system_code_type='30' and system_code_value = 'CANCEL');


UPDATE public.system_config
set value='https://gold.radixhealth.com/gold_db_21_1_0/dashutils/#/cancel'
WHERE "key"='CancelUrl';

UPDATE public.system_config
SET value='https://gold.radixhealth.com/gold_db_21_1_0/dashutils/#/reschedule'
WHERE "key"='RescheduleUrl';

UPDATE public.system_config
SET   value='https://gold.radixhealth.com/gold_db_21_1_0/dashutils/#/confirm'
WHERE "key"='ConfirmUrl';

UPDATE public.system_config
SET  value='https://gold.radixhealth.com/gold_db_21_1_0'
WHERE "key"='RadixSiteBaseURL';


UPDATE public.system_config
SET value='{
	"auth": "true",
	"password": "Da2YagCiWZZ0PSENsKUjuDD9cGGL/YrYPsYYTLg7u82Kk1obro0QTUfCZHYTEaJfPZp7n+3FlAi7rjkIakTAn/yqGw1Jvb+k5aJiCz1LIul82DvM",
	"start_tls": "true",
	"user_name": "AKIAS7SUKMM24RF7FM4C",
	"server_name": "email-smtp.us-east-1.amazonaws.com",
	"server_port": "587",
	"sender_email": "Golddb<Golddb@rhapp.co>"
}'
WHERE "key"='smtpConfigClientGeneralEmail';


UPDATE public.system_config
SET value='{
	"auth": "true",
	"password": "Da2YagCiWZZ0PSENsKUjuDD9cGGL/YrYPsYYTLg7u82Kk1obro0QTUfCZHYTEaJfPZp7n+3FlAi7rjkIakTAn/yqGw1Jvb+k5aJiCz1LIul82DvM",
	"start_tls": "true",
	"user_name": "AKIAS7SUKMM24RF7FM4C",
	"server_name": "email-smtp.us-east-1.amazonaws.com",
	"server_port": "587",
	"sender_email": "Golddb<Golddb@rhapp.co>"
}'
WHERE "key"='smtpConfigPatientAppointmentEmail';

UPDATE public.system_config
SET  value='{
	"auth": "true",
	"password": "Da2YagCiWZZ0PSENsKUjuDD9cGGL/YrYPsYYTLg7u82Kk1obro0QTUfCZHYTEaJfPZp7n+3FlAi7rjkIakTAn/yqGw1Jvb+k5aJiCz1LIul82DvM",
	"start_tls": "true",
	"user_name": "AKIAS7SUKMM24RF7FM4C",
	"server_name": "email-smtp.us-east-1.amazonaws.com",
	"server_port": "587",
	"sender_email": "Golddb<Golddb@rhapp.co>"
}'
WHERE "key"='smtpConfigRadixSupportEmail';

UPDATE public.system_config
SET  value='{
	"vendor": "twillio",
	"phone": "+14043902477",
	"auth-token": "a65c5b07adec290ebf47b122b207a165",
	"account-sid": "AC9202e1aae27507a4d0fc20d639f93fbc"
}'
WHERE "key"='textMessagingAPIConfig';

update
	public.system_config
set
	value = '6LfUFdwUAAAAAObYt_7huAF6MI209OOjC3nLZ_3E'
	where
	"key" = 'GoogleRecaptchaApiKey';


update
	public.system_config
set
	value = '6LfUFdwUAAAAAM3uC4VHCVPtIw275BayyS8MPW-N'
	where
	"key" = 'GoogleRecaptchaApiSecretKey';


update
	public.system_config
set
	value = 'False'
	where
	"key" = 'RECAPTCHA_ENABLED';
